// Função para atualizar o carrinho quando a página é carregada
window.onload = function() {
    atualizarCarrinho();
};

// Função para remover um produto do carrinho
function removerDoCarrinho(id) {
    let carrinho = JSON.parse(localStorage.getItem('carrinho')) || [];

    // Encontrar o índice do produto no carrinho com base no ID
    let index = carrinho.findIndex(item => item.id === id);

    if (index !== -1) {
        // Remover o produto do carrinho
        carrinho.splice(index, 1);

        // Atualizar o carrinho no localStorage
        localStorage.setItem('carrinho', JSON.stringify(carrinho));

        // Atualizar a exibição do carrinho na página
        atualizarCarrinho();
    }
}

// Função para atualizar a exibição do carrinho na página
function atualizarCarrinho() {
    let carrinhoElement = document.getElementById('carrinhoElement');
    let carrinho = JSON.parse(localStorage.getItem('carrinho')) || [];
    let total = 0; // Variável para armazenar o total da compra

    carrinhoElement.innerHTML = '';

    carrinho.forEach(function (produto) {
        let produtoHTML = `
            <tr>
                <td>
                    <div class="product">
                        <img src="${produto.imagem}" alt="${produto.nome}" />
                        <div class="info">
                            <div class="name">${produto.nome}</div>
                        </div>
                    </div>
                </td>
                <td>R$ ${produto.preco}</td>
                <td>
                    <div class="qty">
                        <button onclick="diminuirQuantidade('${produto.id}')"><i class="bx bx-minus"></i></button>
                        <span>${produto.quantidade}</span>
                        <button onclick="aumentarQuantidade('${produto.id}')"><i class="bx bx-plus"></i></button>
                    </div>
                </td>
                <td>R$ ${produto.preco * produto.quantidade}</td>
                <td>
                    <button class="remove" onclick="removerDoCarrinho('${produto.id}')"><i class="bx bx-x"></i></button>
                </td>
            </tr>
        `;

        carrinhoElement.innerHTML += produtoHTML;

        // Adicionar o valor do produto ao total
        total += produto.preco * produto.quantidade;
    });

    // Selecionar o elemento dentro da classe 'box' para exibir o total
    let totalElement = document.querySelector('.box footer span:last-child');
    
    // Atualizar o conteúdo do elemento com o valor total formatado
    totalElement.textContent = `R$ ${total.toFixed(2)}`;
}

// Função para diminuir a quantidade de um produto
function diminuirQuantidade(id) {
    alterarQuantidade(id, -1);
}

// Função para aumentar a quantidade de um produto
function aumentarQuantidade(id) {
    alterarQuantidade(id, 1);
}

// Função para alterar a quantidade de um produto
function alterarQuantidade(id, quantidadeAlteracao) {
    let carrinho = JSON.parse(localStorage.getItem('carrinho')) || [];

    // Encontrar o índice do produto no carrinho com base no ID
    let index = carrinho.findIndex(item => item.id === id);

    if (index !== -1) {
        // Alterar a quantidade do produto
        carrinho[index].quantidade += quantidadeAlteracao;

        // Garantir que a quantidade não seja menor que 1
        if (carrinho[index].quantidade < 1) {
            carrinho[index].quantidade = 1;
        }

        // Atualizar o carrinho no localStorage
        localStorage.setItem('carrinho', JSON.stringify(carrinho));

        // Atualizar a exibição do carrinho na página
        atualizarCarrinho();
    }
}

